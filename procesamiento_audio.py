import os
import subprocess
import speech_recognition as sr
from os import remove

def procesar_audio(audio_bytes):
    flag = 0
    def remover():
        remove(r'opcion_usuario.wav')
        remove(r'opcion_usuario.weba')
        
    def conversion():
        try:
            remover()
            with open('opcion_usuario.weba', mode='bx') as f:
                f.write(audio_bytes)     
            subprocess.call(['ffmpeg', '-i', 'opcion_usuario.weba', '-vn', 'opcion_usuario.wav'])
        except:
            with open('opcion_usuario.weba', mode='bx') as f:
                f.write(audio_bytes)     
            subprocess.call(['ffmpeg', '-i', 'opcion_usuario.weba', '-vn', 'opcion_usuario.wav'])
            
    def reconigtion():
        r = sr.Recognizer()
        audio = sr.AudioFile('opcion_usuario.wav')
        with audio as source:
            print("escuchando...")
            #voz = r.listen(source, phrase_time_limit = 7)
            r.adjust_for_ambient_noise(source)
            voz = r.record(source)
        opcion = r.recognize_google(voz, language='ES-MX')
        print(opcion)
        return opcion
        
    try:
        remover()
    except:
        conversion()
        try:
            opc = reconigtion()
        except:
            flag = 1
            opc = 0
            return flag, opc
    else:
        conversion()
        try:
            opc = reconigtion()
        except:
            flag = 1
            opc = 0
            return flag, opc
        
    remover()
    
    return flag, opc