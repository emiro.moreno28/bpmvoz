preguntas = [
    'Asigne nombre al proceso ', # los dos primeros pasos se piden una sóla vez
    'Seleccione el número de pantallas ',
    'Seleccione los métodos para la pantalla ',# estos pasos se repiten dependiendo del número de pantallas...
    'Asigne nombre a la pantalla ',
    'Seleccione el número de campos de la pantalla ',
    'Nombre del campo ', 
    'Elija el tipo de campo ',
    'El campo es obligatorio? ', 
    'Seleccione la pantalla ', # .....hasta aquí 
    'Desea crear el proceso?',
    'Se ha eliminado el proceso',
    'Se ha creado el proceso'
]

errores = [
    'Palabra no identificada, por favor inténtelo nuevamente', 
    'Número no identificado, por favor dígalo nuevamente',
]