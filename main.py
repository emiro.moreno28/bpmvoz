import os
import time
import cv2
import graphviz
import base64
from flask import Flask, render_template
from flask_socketio import SocketIO, emit
from os import remove
from graphviz import Digraph, Graph
from unidecode import unidecode
from procesamiento_audio import procesar_audio
from asistente import preguntas, errores
from obj_json import fill
#Instalar graphviz: windows_10_cmake_Release_graphviz-install-4.0.0-win64

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret'
socketio = SocketIO(app)

@app.route('/')
def index():
    return render_template('index.html')

audio_bytes = 0

@socketio.on('escuchar')
def escuchar(blob):
    global audio_bytes
    audio_bytes = blob
    
@socketio.on('event')
def comunicacion():
    
    def remover():
        try:
            remove(r'cluster.png')
            remove(r'cluster')
        except:
            pass
    
    def send_img(borrar=0):
        if borrar == 0:
            filepath = "cluster.png"
            image = cv2.imread(filepath) 
            height, width = image.shape[:2]
        
            with open("cluster.png", "rb") as f: 
                image_data = f.read() 
        
            emit('diagrama', {'height':height, 'width':width, 'image_data':image_data, 'borrar':borrar})   
        if borrar == 1: emit('diagrama', {'borrar':borrar})
    
    remover()
    
    p = Digraph('G', filename='cluster',format='png')
    p.graph_attr = dict(newrank = 'true', nodesep = '1', ranksep = '0.3', overlap = 'true', splines = 'ortho')
    
    def opc_usr():
        emit('grabar')
        time.sleep(6)
        flag, opcion = procesar_audio(audio_bytes)
        time.sleep(1)
        print(str(flag))
        while flag == 1:
            msg_err = errores[0]
            emit('voz', msg_err)
            time.sleep(3)
            emit('grabar')
            time.sleep(6)
            flag, opcion = procesar_audio(audio_bytes)
            time.sleep(1)
        
        return opcion
        
    with p.subgraph(name='cluster_bs1',node_attr={'shape': 'Mrecord'}) as c:
        c.attr(label='Wizard')
        with c.subgraph(name='cluster_bs2',node_attr={'shape': 'Mrecord'}) as d:
            d.attr(label='Asistente')
            d.node(name='a0', label='Inicio', shape='circle')
            d.node(name='a1', label='Información del proceso', shape='folder')
            d.edge('a0', 'a1')
    p.render()
    
    send_img()
            
    emit('voz', preguntas[0])
    time.sleep(3)
    
    nombrep = opc_usr()
    namep = unidecode (nombrep.replace(" ","_"))
    fill(1, namep)
    
    emit('voz', preguntas[1])
    time.sleep(3)
    
    bit1 = 0
    while bit1 == 0:
        try:        
            numtt = opc_usr()
            numttint = int(numtt)
            bit1 = 1
        except ValueError:
            emit('voz', errores[1])
            time.sleep(3)
    with p.subgraph(name='cluster_bs',node_attr={'shape': 'Mrecord'}) as c:
        c.attr(label='Usuario')
        c.node(name='opu1', shape='folder', label='<<TABLE BORDER="0" CELLBORDER="1" CELLSPACING="0" CELLPADDING="5"><TR><TD rowspan="1"><b>Proceso: </b></TD><TD rowspan="1">'+ nombrep +'</TD></TR><TR><TD><b>Pantallas:</b></TD><TD>'+ numtt +'</TD></TR></TABLE>>')
    p.edge('a1', 'opu1')
    p.render()
    
    send_img(1)
    send_img()
    
    cont = 0
    cont_json = 1
    l_typesFields = []
    for i in range(numttint):
        cont +=1
        table = {}
        
        with p.subgraph(name='cluster_bs1',node_attr={'shape': 'Mrecord'}) as c:
            with c.subgraph(name='cluster_bs2',node_attr={'shape': 'Mrecord'}) as d:
                d.node(name='a2' + str(cont), label='Parametros de la pantalla ' + str(cont), shape='folder')
        if cont == 1: p.edge('opu1', 'a21')
        if cont -1 >= 1: p.edge('a2' + str(cont-1), 'a2' + str(cont))
        p.render()
        
        send_img(1)
        send_img()
            
        emit('voz', preguntas[2] + str(cont))
        time.sleep(3)
            
        l1 = []
        lj = []
        apisvc = opc_usr()
        apisvclist = apisvc.split()
            
        for j in apisvclist:
            if j == str('creación'): lj.append(j); l1.append('Cr')
            if j == str('eliminación'): lj.append(j); l1.append('Del')
            if j == str('consulta'): lj.append(j); l1.append('Rd')
            if j == str('edición'): lj.append(j); l1.append('Up')
            
        strl1 = " ".join(l1)
        metodos = unidecode (strl1.replace(" ",", "))
        table.update({'apiservices':lj})
        #fill(2, lj)
        
        emit('voz', preguntas[3] + str(cont))
        time.sleep(3)
            
        namet = opc_usr()
        table.update({'table_name':"p" + namet})
        
        #fill(2, table)
        
        emit('voz', preguntas[4])
        time.sleep(3)
        
        bit2 = 0
        while bit2 == 0:
            try:        
                nft = opc_usr()
                nftint = int(nft)
                bit2 = 1
            except ValueError:
                emit('voz', errores[1])
                time.sleep(3)
        
        cont2 = 0
        l2 = []
        l_fields =[]
        for k in range(nftint):
            field = {
                "name": "",
			    "data_type": "",
			    "notNull": None
            }
            
            typeField = {
                "column_name": "",
                "data_type": "",
                "is_nullable": "",
                "table_name": "t_" + namet 
            }
            
            cont2 +=1
            emit('voz', preguntas[5] + str(cont2))
            time.sleep(3)
                    
            namef = opc_usr()
        
            field['name'] = namef   
            typeField['column_name'] = namef
                    
            emit('voz', preguntas[6] + str(cont2))
            time.sleep(3)
                    
            #print("______________________________________________________________________________")
            #print("Texto\nNúmero\nFecha")
                    
            tipof = opc_usr()
            
            if tipof == str('texto'): field['data_type'] = "text"; typeField['data_type'] = "text"
            if tipof == str('número'): field['data_type'] = "Numeric"; typeField['data_type'] = "Numeric"
            if tipof == str('fecha'): field['data_type'] = "date"; typeField['data_type'] = "date"
                    
            emit('voz', preguntas[7])
            time.sleep(3)
                    
            null = opc_usr()
            if null == str('sí'): field['notNull'] = True; typeField['is_nullable'] = "NO"
            if null == str('no'): field['notNull'] = False; typeField['is_nullable'] = "YES"
            
            l_fields.append(field)
            l_typesFields.append(typeField)
            
            campo = '<b>'+ namef +'</b>, <b>Tipo: </b>'+ tipof +', <b>Obligatorio: </b>'+ null +''
            l2.append(campo)
        
        table.update({'fields':l_fields})
        fill(2, table)
        campos = "<br/>".join(l2)
        
        with p.subgraph(name='cluster_bs',node_attr={'shape': 'Mrecord'}) as c:
            c.node(shape='folder', name='opu2'+ str(cont), label='<<TABLE BORDER="0" CELLBORDER="1" CELLSPACING="0" CELLPADDING="5"><TR><TD colspan="1"><b>'+ namet +': </b>'+ metodos +'</TD></TR><TR><TD>'+ campos +'</TD></TR></TABLE>>')
        p.edge('a2' + str(cont), 'opu2' + str(cont))
        p.render()
        
        send_img(1)
        send_img()
        
    fill(3, l_typesFields)

    with p.subgraph(name='cluster_bs1',node_attr={'shape': 'Mrecord'}) as c:
        with c.subgraph(name='cluster_bs2',node_attr={'shape': 'Mrecord'}) as d:
            d.node(name='a3', label='Orden de las pantallas', shape='folder')
    p.edge('a2' + str(cont), 'a3')
    p.render()
    
    send_img(1)
    send_img()

    cont3 = 0
    l3 = []
    l4 = []
    for l in range(numttint):
        cont3 +=1
        
        emit('voz', preguntas[8] + str(cont3))
        time.sleep(3)
        
        pantll = opc_usr()
        
        numpant = '<TD COLSPAN="1"><b>Pantalla_'+ str(cont3) +'</b></TD>'
        l3.append(numpant)
        
        namepant = '<TD>'+ pantll +'</TD>'
        l4.append(namepant)

    colheader = "".join(l3)
    colcontent = "".join(l4)

    with p.subgraph(name='cluster_bs',node_attr={'shape': 'Mrecord'}) as c:
        c.node(shape='folder', name='opu3', label='<<TABLE BORDER="0" CELLBORDER="1" CELLSPACING="0" CELLPADDING="5"><TR>'+ colheader +'</TR><TR>'+ colcontent +'</TR></TABLE>>')
    p.edge('a3', 'opu3')

    with p.subgraph(name='cluster_bs1',node_attr={'shape': 'Mrecord'}) as c:
        c.node(shape='folder', name='w0', label='Crear tablas')
        c.node(shape='doublecircle', name='w1', label='Fin')
        c.edge('a3', 'w0')
        c.edge('w0', 'w1')
    p.render()
    
    #send_img(1)
    #send_img()
    
    emit('voz', preguntas[9])
    time.sleep(3)
    
    confirmar = opc_usr()
    
    #if confirmar == str('sí'):  fill(3, l_typesFields); emit('voz', preguntas[11]); time.sleep(3)
    if confirmar == str('sí') or confirmar == str('crear'):  send_img(1); send_img(); emit('voz', preguntas[11]); time.sleep(3)
    if confirmar == str('no') or confirmar == str('cancelar'):  send_img(1); emit('voz', preguntas[10]); time.sleep(3)
    
if __name__ == '__main__':
    app.run(port=8080,debug=True)