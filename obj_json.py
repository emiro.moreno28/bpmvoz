import requests
import json
from unidecode import unidecode

a = {
	"consecutive": "",
	"grid": 1,
	"idProject": 148,
	"listName": "",
	"listsSelected": [],
	"listsSelectedSons": [],
	"listType": "",
	"nameModule": "", #nombre proceso
	"numSons": 0,
	"project": "next",
	"relationParamsFather": [],
	"relationParamsList": [],
	"relationParamsSon": [],
	"searchParamsFather": [],
	"showParamsList": [],
	"state": 0,
	"table": "",
	"tableFather": "",
	"tableList": {},
	"tableSon": [],
	"typeGrid": "mate",
	"typeScreenFront": "subdivision",
	"typesFields": [],
	"typesFieldsFather": [],
	"typesFieldsSon": [],
    "tables": []
}

def fill(paso,datos):
    global a
    if paso == 1:
        a["nameModule"]= datos
        #print(a)
        
    elif paso == 2:        
        table = {
			"nameTable": "",
			"fields": [],
			"hasRelations": {
				"active": False,
				"relations": ""
			},
			"apiServices": {
				"create": {},
				"delete": {},
				"read": {},
				"update": {}
			},
			#"isFather": True
		}
        
        api_svc_list = datos['apiservices']
        table_name = datos['table_name']
        fields = datos['fields']
        
        relationParamFather = {
			"column_name": "id_" + table_name,
			"data_type": "integer",
			"is_nullable": "NO",
			"pkey": True,
			"table_name": "t_" + table_name
		}
        
        searchParamsFather = {
			"column_name": "id_" + table_name,
			"data_type": "integer",
			"is_nullable": "NO",
			"table_name": "t_" + table_name
		}
        
        for i in api_svc_list:
            if i == str('creación'): table['apiServices']['create'] = {'active': True}   
            if i == str('eliminación'): table['apiServices']['delete'] = {'active': True}                
            if i == str('consulta'): table['apiServices']['read'] = {'active': True}                
            if i == str('edición'): table['apiServices']['update'] = {'active': True}
        
        table["nameTable"] = table_name
        table["fields"] = fields
        
        a['tables'].append(table)
        a['relationParamsFather'].append(relationParamFather)
        a['searchParamsFather'].append(searchParamsFather)
        
        #print(a)
        
    elif paso == 3:
        typesFields = datos
        a['typesFields'] = typesFields
        #print(a)
        
        url='http://186.29.195.12:4001/getHash'
        # url = 'http://localhost:4000/api/postgres/project/getHash'
        data = requests.post(url)
        data = data.json()
        a['consecutive'] =data['hash']
        json_a = json.dumps(a) # ajustar a formato json
        
        print(json_a)
        
    else:
        ''' url='http://186.29.195.12:4001/getHash'
        data = requests.post(url)
        data = data.json()
        a['consecutive'] =data['hash']
        json_a = json.dumps(a) # ajustar a formato json
        
        print(json_a) '''
        return